import {axiosInstance} from 'boot/axios'
import {Notify} from 'quasar'
import qs from 'qs'

export function getAll(state,{pagination,filter}) {
  return new Promise((resolve, reject) => {
    return axiosInstance.get('/').then(response => {
      state.commit('setUsers',response.data)
      resolve(response.data)
    }).catch(error => {
      if (error.response) {
        Notify.create({color: 'negative', message: error.response.data.message})
      } else {
        Notify.create({color: 'negative', message: 'Something happened, Try again'})
      }
      reject(error)
    })
  });
}
export function getUser(state,id){
  return new Promise((resolve,reject)=>{
    return axiosInstance.get('/user/'+id).then((response)=>{
      resolve(response.data)
    }).catch((error)=>{
      if (error.response) {
        Notify.create({color: 'negative', message: error.response.data.message})
      } else {
        Notify.create({color: 'negative', message: 'Something happened, Try again'})
      }
      reject(error)
    })
  })
}
export function createUser(state,data){
  return new Promise((resolve,reject)=>{
    return axiosInstance.post('/user',data).then((response)=>{
      resolve()
    }).catch((error)=>{
      console.log(error.response)
      if (error.response.data) {
        state.commit('userErrors', {hasError: true, errors: error.response.data.message})
        Notify.create({color: 'negative', message: error.response.data.message})
      } else {
        Notify.create({color: 'negative', message: 'Something happened, Try again'})
      }
      reject(error)
    })
  })
}
export function updateUser(state,{data,id}){
  return new Promise((resolve,reject)=>{
    return axiosInstance.put('/user/'+id,data).then((response)=>{
      resolve()
    }).catch((error)=>{
      if (error.response) {
        state.commit('userErrors', {hasError: true, errors: error.response.data.message})
        Notify.create({color: 'negative', message: error.response.data.message})
      } else {
        Notify.create({color: 'negative', message: 'Something happened, Try again'})
      }
      reject(error)
    })
  })
}
export function deleteUser(state,id){
  return new Promise((resolve,reject)=>{
    return axiosInstance.delete('/user/'+id).then((response)=>{
      resolve()
    }).catch((error)=>{
      if (error.response) {
        Notify.create({color: 'negative', message: error.response.data.message})
      } else {
        Notify.create({color: 'negative', message: 'Something happened, Try again'})
      }
      reject(error)
    })
  })
}
