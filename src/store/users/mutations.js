
export function setUsers (state,data) {
  state.users=data
}

export function userErrors(state, data) {
  state.errors.errors = data.errors
  state.errors.hasError = data.hasError
}
