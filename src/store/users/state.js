export default function () {
  return {
    users:[],
    user:{},
    errors: {
      hasError: false,
      errors: {}
    },
    paginate: {
      page: 1,
      descending: true,
      sortBy: 'id'
    }
  }
}
