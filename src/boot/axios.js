import axios from 'axios'
import {Notify} from 'quasar'

const axiosInstance = axios.create({})

axiosInstance.defaults.baseURL = 'http://ec2-54-211-141-208.compute-1.amazonaws.com/'
axiosInstance.defaults.serverBaseUrl = 'http://ec2-54-211-141-208.compute-1.amazonaws.com/'
axiosInstance.interceptors.response.use((response) => {
  return response
}, (error) => {
  if (!error.response) {
    Notify.create({color:'negative',message:'network error'})
  }
  return Promise.reject(error)
})
export default ({Vue}) => {
  Vue.prototype.$axios = axiosInstance
  Vue.axios = Vue.prototype.$axios
}
export {axiosInstance}
